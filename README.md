V-Trommelbremse
===============


Was ist das?
------------

Eine verbesserte Version der Sturmey&Archer 70 mm Trommelbremse, zur Verwendung mit den dazu passenden Bremstrommeln und Bremsbelägen.



Warum?
------

Die Original-Bremse hat ein paar Nachteile:

* Schwergängig: In der Original-Bremse wird die lineare Bewegung des Bremszuges in eine Drehbewegung des Bremsnockens umgesetzt, dieser wiederum drückt die Bremsbeläge auseinander. Dabei gleitet der Bremsnocken auf dem Bremsschuh, was viel Reibung erzeugt, speziell mit Verschmutzung.

* Rückstellung bei Belagverschleiß schlecht: Wenn sich die Bremsbeläge abnutzen, muss sich der Bremsnocken weiter drehen, bis die Bremsbeläge auf der Trommel anliegen. Aber je stärker er sich dreht, desto größer wird die Seitwärts-Komponente der Bewegung – und die Rückstellfeder schafft es nicht mehr, beim Zusammendrücken der Bremsbeläge diese Seitwärtsbewegung aufzubringen und die Bremse blockiert. Als Abhilfe kann man dünne Blechstreifen unter die Gleitschuhe legen, die die verschlissene Belagdicke kompensieren.

* Schwer: Die Konstruktion ist nicht gerade Leichtbau.


Der neue Entwurf funktioniert nach dem Prinzip der V-Brake – die Bewegung des Bremszugs wirkt über Hebel direkt auf die Bremsbeläge; da diese auseinander- statt zusammengedrückt werden müssen, müssen sich die Hebel kreuzen. Das bedeutet:

* Es gibt keine Umsetzung über eine Drehbewegung mehr, und damit keine Seitwärtsbewegung, die Reibung verursacht.

* Damit entfällt auch das Unterfüttern der Gleitschuhe – der Belagverschleiß beeinflusst nicht mehr die Rückstellung der Bremsbeläge, man muss nur noch den Bremszug nachstellen, um den Verschleiß auszugleichen.

* Da der Bremszug nicht am Federbein befestigt ist, sondern nur noch zwischen den Hebeln der Bremse, können diese beliebig ausgerichtet sein. Das ermöglicht Bremszüge, die kürzer und weniger gekrümmt sind, was die Reibung reduziert.

* Da der Bremszug nicht mehr senkrecht von unten nach oben verlaufen muss, gibt es bei Kälte seltener ein Problem mit gefrierender Feuchtigkeit in der Außenhülle.

* Die Konstruktion ist einfacher, es kann etwas Gewicht gespart werden (ca. 160 g vs. 245 g).



Material
--------

* 1x Grundplatte
* 1x Bremshebel 1, langes Teil
* 1x Bremshebel 1, kurzes Teil
* 1x Bremshebel 2
* 2x Sechskant-Gewindehülse mit Schlitz (= Bremsbelag-Halter)
* 1x kurzes Rohrstück, Innendurchmesser 12 mm, Länge 10 mm (= Achs-Abstandhalter)
* 2x Schulterschraube M3 mit 3mm hoher Schulter, 4 mm Durchmesser (= )
* 1x Schraube M5x6 (= Verbindung der beiden Teile von Bremshebel 1)
* 2x Schraube M6x12 (= Befestigung Bremsbelaghalter auf Bremshebel)
* 1x Hülsenmutter mit Flansch M6x18 (= Gelenk Bremsbeläge)
* 2x Linsenkopf-Schraube M6x6 mit Flansch (= Gelenk Bremsbeläge + Bremszug-Klemmung)



Zusammenbau
-----------

* herausfinden, wo der Bremshebel 1 (= mit Anschlag für die Bremszug-Außenhülle) befestigt werden muss, damit die Zugführung stimmt (ist bei linker und rechter Bremse unterschiedlich!)
* => Bremshebel 1 muss in die Richtung zeigen, von der der Bremszug kommt; entsprechend muss der Zuganschlag so gebogen sein, dass er in Richtung Bremshebel 2 zeigt
* Bremshebel 1, langes Teil: Bremszug-Anschlag-Lasche in die benötigte Richtung um 90° umbiegen (z.B. mit Schraubstock und Hammer)
* Bremshebel 1: beide Teile mit M5-Schraube zusammenschrauben; so, dass der kurze Teil auf der gleichen Seite des langen Teils sitzt wie die Biegung der Zuganschlag-Lasche
* beide Bremshebel: Bremsbelag-Halter mit M6-Schrauben anschrauben:
  * so, dass die Bremsbelag-Halter auf der Seite der Bremshebel sind, auf der auch die Zuganschlag-Lasche bei Bremshebel 1 ist
  * so, dass jeweils das lange Stück neben dem Schlitz am Bremshebel angeschraubt ist
  * so, dass der Schlitz von der Biegung im Bremshebel weg zeigt und dass der Bremsbelag-Halter so gedreht ist
  * so, dass der Bremsbelag-Halter bündig mit dem Bremshebel ist (d.h. die Kante des Sechsecks parallel zur Kante des Bremshebels)
* beide Bremshebel an die Grundplatte anschrauben; so, dass die Grundplatte auf der selben Seite der Bremshebel wie die Bremsbelag-Halter ist
* Hülsenmutter mit Flansch durch die Grundplatte stecken
* Bremsbeläge auf die Hülsenmutter stecken und mit Linsenkopf-Schraube sichern
* Ende der Bremsbeläge (ohne Gleitschuhe) in die Schlitze der Bremsbelag-Halter stecken
* Feder der Bremsbeläge zwischen Bremsbeläge und Grundplatte einfädeln und in Bremsbeläge einhängen
* Bremse auf Federbein-Achse stecken
* Rohrstück als Abstandhalter auf die Federbein-Achse stecken
* Drehmomentstütze zwischen Federbein und Bremse (Bremsbelag-Gelenk) schrauben; so, dass sie auf der den Bremsbelägen entgegengesetzten Seite der Grundplatte befestigt ist
* Bremszug durch die Zuganschlag-Lasche stecken
* Innenzug auf dem anderen Bremshebel 2 mit Linsenkopf-Schraube festklemmen



Fertigung
---------

* Bremsen-Bestandteile aus 3 mm starkem Edelstahl (z.B. 1.4404) lasern lassen.
  * Hierzu kann man die DXF-Dateien aus dem Repository verwenden.
  * Erstellt wurde die Konstruktionszeichnung mit [*FreeCAD*](https://www.freecadweb.org/).
  * Die DXF-Dateien sind aus *FreeCAD* exportiert worden.
* Löcher in Grundplatte für Hebel-Scharniere: mit Gewindebohrer jeweils ein M3-Gewinde schneiden
* Loch im langen Teil von Bremshebel 1: mit Gewindebohrer ein M5-Gewinde schneiden
* Loch am Ende von Bremshebel 2: mit Gewindebohrer ein M6-Gewinde schneiden
* Bremsbelag-Halter: mit Flex einen 5 mm breiten und 7 mm tiefen Schlitz hineinschneiden; 10 mm von einem Ende entfernt
* Drehmomentstütze: Blechstreifen, ca. 10 mm breit, ca. 2.5 mm dick, mit zwei Bohrungen 6 mm


Lizenz
------

[*Creative Commons CC-BY*](https://creativecommons.org/licenses/by/4.0/deed.de)
– kurz gesagt: Alles darf beliebig weiterverwendet werden, aber die Autoren müssen genannt werden.
